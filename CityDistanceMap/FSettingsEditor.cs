﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CityDistanceMap
{
	public partial class FSettingsEditor : Form
	{
		public FSettingsEditor()
		{
			InitializeComponent();
		}

		private void closeB_Click(object sender, EventArgs e)
		{
			Var.SaveProject(Var.ProjectPath);
			this.Hide();
		}

		public void RefreshInfo()
		{
			titleTB.Text = Var.Project.Title;
			subtitleTB.Text = Var.Project.Subtitle;
			legendPositionCB.SelectedIndex = Var.Project.LegendPosition - 1;
			mapFilenameTB.Text = Var.Project.MapFilename;
			nameChB.Checked = Var.Project.ShowCaptions;
			valueChB.Checked = Var.Project.ShowValues;
			legendChB.Checked = Var.Project.ShowLegend;
			connectionsChB.Checked = Var.Project.ShowCityLegendConnection;
			gravityChB.Checked = Var.Project.ShowCityGravity;
		}

		private void saveB_Click(object sender, EventArgs e)
		{
			Var.Project.Title = titleTB.Text;
			Var.Project.Subtitle = subtitleTB.Text;
			Var.Project.LegendPosition = (byte) (legendPositionCB.SelectedIndex + 1);
			Var.Project.MapFilename = mapFilenameTB.Text;
			Var.Project.ShowCaptions = nameChB.Checked;
			Var.Project.ShowValues = valueChB.Checked;
			Var.Project.ShowLegend = legendChB.Checked;
			Var.Project.ShowCityLegendConnection = connectionsChB.Checked;
			Var.Project.ShowCityGravity = gravityChB.Checked;
			//
			if (!Var.Project.ShowLegend && Var.Project.ShowCityLegendConnection)
				MessageBox.Show("Warning!\n\nSince you do not want to show the legend, the city-legend interval connections will also not be printed.");
			//
			saveB.Enabled = false;
			saveB.Text = "Saved!";
			saveT.Enabled = true;
		}

		private void saveT_Tick(object sender, EventArgs e)
		{
			saveT.Enabled = false;
			saveB.Enabled = true;
			saveB.Text = "Save";
		}

		private void selectMapB_Click(object sender, EventArgs e)
		{
			openD.ShowDialog();
		}

		private void openD_FileOk(object sender, CancelEventArgs e)
		{
			mapFilenameTB.Text = Path.GetFileName(openD.FileName);
		}
	}
}
