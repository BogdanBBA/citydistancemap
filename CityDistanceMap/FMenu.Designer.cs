﻿namespace CityDistanceMap
{
	partial class FMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMain));
			this.closeB = new System.Windows.Forms.Button();
			this.logTB = new System.Windows.Forms.TextBox();
			this.openB = new System.Windows.Forms.Button();
			this.processB = new System.Windows.Forms.Button();
			this.openD = new System.Windows.Forms.OpenFileDialog();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.editIntervalsB = new System.Windows.Forms.Button();
			this.editCitiesB = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.editSettingsB = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// closeB
			// 
			this.closeB.Font = new System.Drawing.Font("Segoe UI", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.closeB.ForeColor = System.Drawing.Color.Black;
			this.closeB.Location = new System.Drawing.Point(454, 85);
			this.closeB.Name = "closeB";
			this.closeB.Size = new System.Drawing.Size(200, 39);
			this.closeB.TabIndex = 0;
			this.closeB.Text = "Exit";
			this.closeB.UseVisualStyleBackColor = true;
			this.closeB.Click += new System.EventHandler(this.closeB_Click);
			// 
			// logTB
			// 
			this.logTB.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.logTB.Location = new System.Drawing.Point(42, 188);
			this.logTB.Multiline = true;
			this.logTB.Name = "logTB";
			this.logTB.ReadOnly = true;
			this.logTB.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.logTB.Size = new System.Drawing.Size(612, 298);
			this.logTB.TabIndex = 1;
			this.logTB.Text = "abcede12334\r\nfdsfsdfd";
			// 
			// openB
			// 
			this.openB.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.openB.ForeColor = System.Drawing.Color.Black;
			this.openB.Location = new System.Drawing.Point(42, 85);
			this.openB.Name = "openB";
			this.openB.Size = new System.Drawing.Size(200, 39);
			this.openB.TabIndex = 2;
			this.openB.Text = "Select project";
			this.openB.UseVisualStyleBackColor = true;
			this.openB.Click += new System.EventHandler(this.openB_Click);
			// 
			// processB
			// 
			this.processB.Enabled = false;
			this.processB.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.processB.ForeColor = System.Drawing.Color.Black;
			this.processB.Location = new System.Drawing.Point(248, 85);
			this.processB.Name = "processB";
			this.processB.Size = new System.Drawing.Size(200, 39);
			this.processB.TabIndex = 3;
			this.processB.Text = "Process";
			this.processB.UseVisualStyleBackColor = true;
			this.processB.Click += new System.EventHandler(this.processB_Click);
			// 
			// openD
			// 
			this.openD.DefaultExt = "xml";
			this.openD.ReadOnlyChecked = true;
			this.openD.Title = "Open project file";
			this.openD.FileOk += new System.ComponentModel.CancelEventHandler(this.openD_FileOk);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Segoe UI Black", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(520, 290);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(112, 45);
			this.label1.TabIndex = 4;
			this.label1.Text = "label1";
			this.label1.Visible = false;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Location = new System.Drawing.Point(520, 327);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(66, 28);
			this.label2.TabIndex = 5;
			this.label2.Text = "label2";
			this.label2.Visible = false;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Location = new System.Drawing.Point(520, 358);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(79, 32);
			this.label3.TabIndex = 6;
			this.label3.Text = "label3";
			this.label3.Visible = false;
			// 
			// editIntervalsB
			// 
			this.editIntervalsB.Enabled = false;
			this.editIntervalsB.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.editIntervalsB.ForeColor = System.Drawing.Color.Black;
			this.editIntervalsB.Location = new System.Drawing.Point(248, 130);
			this.editIntervalsB.Name = "editIntervalsB";
			this.editIntervalsB.Size = new System.Drawing.Size(200, 39);
			this.editIntervalsB.TabIndex = 7;
			this.editIntervalsB.Text = "Edit intervals";
			this.editIntervalsB.UseVisualStyleBackColor = true;
			this.editIntervalsB.Click += new System.EventHandler(this.editIntervalsB_Click);
			// 
			// editCitiesB
			// 
			this.editCitiesB.Enabled = false;
			this.editCitiesB.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.editCitiesB.ForeColor = System.Drawing.Color.Black;
			this.editCitiesB.Location = new System.Drawing.Point(454, 130);
			this.editCitiesB.Name = "editCitiesB";
			this.editCitiesB.Size = new System.Drawing.Size(200, 39);
			this.editCitiesB.TabIndex = 8;
			this.editCitiesB.Text = "Edit cities";
			this.editCitiesB.UseVisualStyleBackColor = true;
			this.editCitiesB.Click += new System.EventHandler(this.editCitiesB_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(38, 48);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(343, 21);
			this.label4.TabIndex = 9;
			this.label4.Text = "Load a project, edit what you will, and export!";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(17, 9);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(262, 30);
			this.label6.TabIndex = 26;
			this.label6.Text = "City distance map creator";
			// 
			// editSettingsB
			// 
			this.editSettingsB.Enabled = false;
			this.editSettingsB.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.editSettingsB.ForeColor = System.Drawing.Color.Black;
			this.editSettingsB.Location = new System.Drawing.Point(42, 130);
			this.editSettingsB.Name = "editSettingsB";
			this.editSettingsB.Size = new System.Drawing.Size(200, 39);
			this.editSettingsB.TabIndex = 27;
			this.editSettingsB.Text = "Edit settings";
			this.editSettingsB.UseVisualStyleBackColor = true;
			this.editSettingsB.Click += new System.EventHandler(this.editSettingsB_Click);
			// 
			// FMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.SteelBlue;
			this.ClientSize = new System.Drawing.Size(697, 513);
			this.ControlBox = false;
			this.Controls.Add(this.editSettingsB);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.editCitiesB);
			this.Controls.Add(this.editIntervalsB);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.processB);
			this.Controls.Add(this.openB);
			this.Controls.Add(this.logTB);
			this.Controls.Add(this.closeB);
			this.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "FMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "v1.0 - May 10th-11th (+19th), 2014 / by BogdanBBA";
			this.Load += new System.EventHandler(this.FMain_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button closeB;
		private System.Windows.Forms.Button openB;
		private System.Windows.Forms.Button processB;
		private System.Windows.Forms.OpenFileDialog openD;
		public System.Windows.Forms.TextBox logTB;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button editIntervalsB;
		private System.Windows.Forms.Button editCitiesB;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button editSettingsB;
	}
}

