﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CityDistanceMap
{
	public partial class FIntervalEditor : Form
	{
		private TInterval currInterval = null;

		public FIntervalEditor()
		{
			InitializeComponent();
		}

		private void closeB_Click(object sender, EventArgs e)
		{
			Var.SaveProject(Var.ProjectPath);
			this.Hide();
		}

		public void RefreshInfo()
		{
			intervalLB.Items.Clear();
			foreach (TInterval interval in Var.Project.Intervals)
				intervalLB.Items.Add(string.Format("Interval ({0}) : {1}", interval.UpToValue, interval.Caption));
			label6.Text = "Intervals (" + intervalLB.Items.Count.ToString() + ")";
			intervalLB_SelectedIndexChanged(null, null);
		}

		private void RefreshColorLabels()
		{
			borderColorL.Text = string.Format("RGB({0}, {1}, {2})", changeBorderColorB.BackColor.R, changeBorderColorB.BackColor.G, changeBorderColorB.BackColor.B);
			fillColorL.Text = string.Format("RGB({0}, {1}, {2})", changeFillColorB.BackColor.R, changeFillColorB.BackColor.G, changeFillColorB.BackColor.B);
		}

		private void RefreshPreviewLabel()
		{
			fontPreviewL.ForeColor = changeFontColorB.BackColor;
			fontPreviewL.Left = panel1.Width / 2 - fontPreviewL.Width / 2;
			fontPreviewL.Top = panel1.Height / 2 - fontPreviewL.Height / 2;
		}

		private void intervalLB_SelectedIndexChanged(object sender, EventArgs e)
		{
			valueTrB.Value = valueTrB.Minimum;
			valueTrB.Enabled = intervalLB.SelectedIndex != -1;
			valueTB.Text = "";
			radiusTrB.Value = radiusTrB.Minimum;
			radiusTrB.Enabled = valueTrB.Enabled;
			radiusTB.Text = "";
			changeBorderColorB.BackColor = Color.White;
			borderColorL.Text = "";
			changeFillColorB.BackColor = Color.White;
			fillColorL.Text = "";
			captionTB.Text = "";
			changeFontB.Enabled = valueTrB.Enabled;
			panel1.Visible = valueTrB.Enabled;
			autoCaptionB.Enabled = valueTrB.Enabled;
			changeFontColorB.BackColor = Color.White;

			delB.Enabled = valueTrB.Enabled;
			saveB.Enabled = valueTrB.Enabled;

			if (intervalLB.SelectedIndex == -1)
				return;

			currInterval = Var.Project.Intervals[intervalLB.SelectedIndex];
			valueTrB.Value = (int) currInterval.UpToValue;
			valueTrB_Scroll(sender, e);
			radiusTrB.Value = currInterval.Radius;
			radiusTrB_Scroll(sender, e);
			changeBorderColorB.BackColor = Var.TColorToColor(currInterval.BorderColor);
			changeFillColorB.BackColor = Var.TColorToColor(currInterval.FillColor);
			RefreshColorLabels();
			captionTB.Text = currInterval.Caption;
			fontPreviewL.Font = Var.TFontToFont(currInterval.CaptionFont);
			changeFontColorB.BackColor = Var.TColorToColor(currInterval.CaptionFont.Color);
			RefreshPreviewLabel();

			valueTB.Focus();
		}

		private void valueTrB_Scroll(object sender, EventArgs e)
		{
			valueTB.Text = valueTrB.Value.ToString();
		}

		private void radiusTrB_Scroll(object sender, EventArgs e)
		{
			radiusTB.Text = radiusTrB.Value.ToString();
		}

		private void changeFontB_Click(object sender, EventArgs e)
		{
			fontD.Font = fontPreviewL.Font;
			if (fontD.ShowDialog() == DialogResult.OK)
			{
				fontPreviewL.Font = fontD.Font;
				RefreshPreviewLabel();
			}
		}

		private void changeBorderColorB_Click(object sender, EventArgs e)
		{
			if (intervalLB.SelectedIndex == -1)
				return;
			colorD.Color = changeBorderColorB.BackColor;
			if (colorD.ShowDialog() == DialogResult.OK)
			{
				changeBorderColorB.BackColor = colorD.Color;
				RefreshColorLabels();
			}
		}

		private void changeFillColorB_Click(object sender, EventArgs e)
		{
			if (intervalLB.SelectedIndex == -1)
				return;
			colorD.Color = changeFillColorB.BackColor;
			if (colorD.ShowDialog() == DialogResult.OK)
			{
				changeFillColorB.BackColor = colorD.Color;
				RefreshColorLabels();
			}
		}

		private void changeFontColorB_Click(object sender, EventArgs e)
		{
			if (intervalLB.SelectedIndex == -1)
				return;
			colorD.Color = changeFontColorB.BackColor;
			if (colorD.ShowDialog() == DialogResult.OK)
			{
				changeFontColorB.BackColor = colorD.Color;
				RefreshPreviewLabel();
			}
		}

		private void saveB_Click(object sender, EventArgs e)
		{
			currInterval.CaptionFont = Var.FontToTFont(fontPreviewL.Font);
			currInterval.CaptionFont.Color = Var.ColorToTColor(fontPreviewL.ForeColor);
			currInterval.Caption = captionTB.Text;
			currInterval.FillColor = Var.ColorToTColor(changeFillColorB.BackColor);
			currInterval.BorderColor = Var.ColorToTColor(changeBorderColorB.BackColor);
			currInterval.Radius = Int32.Parse(radiusTB.Text);
			currInterval.UpToValue = Double.Parse(valueTB.Text);

			intervalLB.Items[intervalLB.SelectedIndex] = string.Format("Interval ({0}) : {1}", currInterval.UpToValue, currInterval.Caption);
			saveB.Enabled = false;
			saveB.Text = "Saved!";
			saveT.Enabled = true;
		}

		private void saveT_Tick(object sender, EventArgs e)
		{
			saveT.Enabled = false;
			saveB.Enabled = true;
			saveB.Text = "Save";
		}

		private void addB_Click(object sender, EventArgs e)
		{
			currInterval = new TInterval();
			currInterval.Caption = "[New interval]";
			currInterval.CaptionFont = Var.FontToTFont(closeB.Font);
			currInterval.CaptionFont.Color = Var.ColorToTColor(Color.Black);
			currInterval.BorderColor = Var.ColorToTColor(Color.Black);
			currInterval.FillColor = Var.ColorToTColor(Color.WhiteSmoke);
			currInterval.Radius = 40;
			currInterval.UpToValue = 100;
			Var.Project.Intervals.Add(currInterval);
			RefreshInfo();
			intervalLB.SelectedIndex = intervalLB.Items.Count - 1;
		}

		private void delB_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to delete the selected interval?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				Var.Project.Intervals.RemoveAt(intervalLB.SelectedIndex);
				RefreshInfo();
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (intervalLB.SelectedIndex == 0)
				captionTB.Text = "... - " + valueTB.Text;
			else
				captionTB.Text = Var.Project.Intervals[intervalLB.SelectedIndex - 1].UpToValue.ToString() + " - " + valueTB.Text;
		}
	}
}
