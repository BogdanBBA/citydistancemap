﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CityDistanceMap
{
	public partial class FMain : Form
	{
		private List<Point> legendIntervalCenters = new List<Point>();

		public FMain()
		{
			InitializeComponent();
		}

		private void closeB_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}

		// On load
		private void FMain_Load(object sender, EventArgs e)
		{
			Var.ProjectsPath = Directory.GetCurrentDirectory() + "\\projects";
			Var.SettingsEditor.Owner = this;
			Var.IntervalEditor.Owner = this;
			Var.CityEditor.Owner = this;
			logTB.Clear();
			log("Application initialized successfully" + Var.NL);
			log("Please remember to only use bitmap (*.bmp) images for projects");
			//Var.CreateInitializatoryProject();
		}

		// Log to text box
		private void log(string Text)
		{
			string s = Text + Var.NL;
			if (!Text.Equals(""))
				s = " ### " + s;
			logTB.AppendText(s);
		}

		// On open file dialog
		private void openB_Click(object sender, EventArgs e)
		{
			openD.InitialDirectory = Var.ProjectsPath;
			openD.Filter = "XML Project|*.xml";
			openD.ShowDialog();
		}

		// On confirm open file
		private void openD_FileOk(object sender, CancelEventArgs e)
		{
			logTB.Clear();
			processB.Enabled = false;
			editSettingsB.Enabled = processB.Enabled;
			editIntervalsB.Enabled = processB.Enabled;
			editCitiesB.Enabled = processB.Enabled;
			try
			{
				Var.ProjectPath = openD.FileName;
				string res = Var.OpenProject(Var.ProjectPath);
				if (!res.Equals(""))
					throw new Exception(res);
			}
			catch (Exception E)
			{
				log("ERROR: " + E.ToString() + Var.NL);
				return;
			}
			//
			label6.Text = "Project \"" + Path.GetFileNameWithoutExtension(Var.ProjectPath) + "\"";
			Var.Project.PrepareData();
			log("The project was read successfully" + Var.NL);
			log("Project data:");
			log(string.Format("Title '{0}', subtitle '{1}', Map filename '{2}', legend pos={3}, captions={4}, values={5}, legend={6}, connections={7}, gravity={8}",
				Var.Project.Title, Var.Project.Subtitle, Var.Project.MapFilename, Var.Project.LegendPosition, Var.Project.ShowCaptions,
				Var.Project.ShowValues, Var.Project.ShowLegend, Var.Project.ShowCityLegendConnection, Var.Project.ShowCityGravity));
			log("\nIntervals (" + Var.Project.Intervals.Count.ToString() + "):");
			foreach (TInterval i in Var.Project.Intervals)
				log(i.ToString());
			log("\nCities (" + Var.Project.Cities.Count.ToString() + "):");
			foreach (TCity city in Var.Project.Cities)
				log(city.ToString());
			//
			processB.Enabled = true;
			editSettingsB.Enabled = processB.Enabled;
			editIntervalsB.Enabled = processB.Enabled;
			editCitiesB.Enabled = processB.Enabled;
		}

		private byte[] getData(string FilePath)
		{
			FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
			BinaryReader br = new BinaryReader(fs);
			byte[] data = br.ReadBytes((int) fs.Length);
			br.Close();
			fs.Close();
			return data;
		}

		// Draw legend
		private void DrawLegend(Bitmap bmp, Graphics g, Pen pen, Brush brush)
		{
			if (Var.Project.ShowLegend)
			{
				log("Drawing legend...");

				int circleStrapWidth = 2 * Var.Project.Intervals[0].Radius + 40, textStrapWidth = 350, totalHeight = 60 + 20;
				foreach (TInterval interval in Var.Project.Intervals)
					totalHeight += 2 * interval.Radius + 20;
				legendIntervalCenters.Clear();

				Point loc = new Point(bmp.Width / 40, bmp.Width / 40);
				Size size = new Size(circleStrapWidth + textStrapWidth, totalHeight);
				if (Var.Project.LegendPosition == TProject.TopRight)
					loc = new Point(bmp.Width - (circleStrapWidth + textStrapWidth) - bmp.Width / 40, bmp.Width / 40);
				if (Var.Project.LegendPosition == TProject.BottomRight)
					loc = new Point(bmp.Width - (circleStrapWidth + textStrapWidth) - bmp.Width / 40, bmp.Height - totalHeight - bmp.Width / 40);
				if (Var.Project.LegendPosition == TProject.BottomLeft)
					loc = new Point(bmp.Width / 40, bmp.Height - totalHeight - bmp.Width / 40);

				pen = new Pen(Color.Black, (int) Math.Round((double) bmp.Height / 200));
				brush = new SolidBrush(Color.White);
				g.FillRectangle(brush, loc.X, loc.Y, size.Width, size.Height);
				g.DrawRectangle(pen, loc.X, loc.Y, size.Width, size.Height);

				int lastTop = loc.Y + 20, left = loc.X + size.Width / 2;
				g.DrawString(Var.Project.Title, label1.Font, Brushes.Black, left - g.MeasureString(Var.Project.Title, label1.Font).Width / 2, lastTop - 10);
				g.DrawString(Var.Project.Subtitle, label2.Font, Brushes.Black, left - g.MeasureString(Var.Project.Subtitle, label2.Font).Width / 2, lastTop + 20);

				lastTop += 60;
				left = loc.X + 20 + circleStrapWidth / 2;
				foreach (TInterval interval in Var.Project.Intervals)
					try
					{
						pen = new Pen(Var.TColorToColor(interval.BorderColor), interval.Radius / 10);
						brush = new SolidBrush(Var.TColorToColor(interval.FillColor));

						g.FillEllipse(brush, left - interval.Radius, lastTop, 2 * interval.Radius, 2 * interval.Radius);
						g.DrawEllipse(pen, left - interval.Radius, lastTop, 2 * interval.Radius, 2 * interval.Radius);

						legendIntervalCenters.Add(new Point(left, lastTop + interval.Radius));

						g.DrawString(interval.Caption, label3.Font, Brushes.Black, new Point(loc.X + circleStrapWidth + 45, lastTop + interval.Radius - label3.Height));

						lastTop += 2 * interval.Radius + 20;
					}
					catch (Exception E)
					{
						MessageBox.Show("ERROR for interval \"" + "\"! Attempting to continue iteration..." + Var.DNL + E.ToString());
					}
			}
		}

		// Process map
		private void processB_Click(object sender, EventArgs e)
		{
			// Init
			logTB.Clear();
			log("Exporting..." + Var.NL);
			string Phase = "";

			// Main export body
			try
			{
				// Init 
				log("Initializing...");
				Phase = "Initializing objects";
				Var.Project.PrepareData();
				Image img = null;
				Bitmap bmp = null;
				Graphics g = null;
				SolidBrush brush = null;
				Pen pen = null;
				string outputPath = string.Format("{0}\\[{1}] {2}.jpg", Var.ProjectsPath, Path.GetFileNameWithoutExtension(Var.ProjectPath), DateTime.Now.ToString("[yyyy-MM-dd] [HH.mm.ss]"));

				log("Reading image...");
				Phase = "Reading bytes from image file";
				byte[] imgData = getData(Var.ProjectsPath + "\\" + Var.Project.MapFilename);
				Phase = "Converting byte data to Image";
				img = Image.FromStream(new MemoryStream(imgData));
				Phase = "Converting Image to Bitmap";
				bmp = new Bitmap(img);
				Phase = "Getting Graphics from Bitmap";
				g = Graphics.FromImage(bmp);

				// Legend (to set the positions of the interval circles)
				DrawLegend(bmp, g, pen, brush);

				// City-legend interval connections and city gravity circles
				if (Var.Project.ShowCityLegendConnection)
					log("Drawing city-legend interval connections...");
				if (Var.Project.ShowCityGravity)
					log("Drawing city gravity circles...");
				foreach (TCity city in Var.Project.Cities)
				{
					// City-legend interval connections 
					if (Var.Project.ShowCityLegendConnection)
					{
						pen = new Pen(Color.Black, 1);
						g.DrawLine(pen, city.GetActualCoords(bmp.Size), legendIntervalCenters[Var.Project.GetIntervalIndexForValue(city.Value)]);
					}
					// City gravity circles
					if (Var.Project.ShowCityGravity)
					{
						TInterval interval = Var.Project.GetIntervalForValue(city.Value);
						pen = new Pen(Color.Black, 1);
						int sizeRatio = 5;
						int x = city.GetActualCoords(bmp.Size).X - sizeRatio * interval.Radius, y = city.GetActualCoords(bmp.Size).Y - sizeRatio * interval.Radius;
						g.DrawEllipse(pen, x, y, sizeRatio * (2 * interval.Radius), sizeRatio * (2 * interval.Radius));
					}
				}

				// City elements
				log("Drawing city circles...");
				Phase = "Drawing city elements";
				if (Var.Project.ShowCaptions)
					log("Drawing city names...");
				if (Var.Project.ShowValues)
					log("Drawing city values...");

				foreach (TCity city in Var.Project.Cities)
					try
					{
						TInterval interval = Var.Project.GetIntervalForValue(city.Value);
						int x = city.GetActualCoords(bmp.Size).X - interval.Radius, y = city.GetActualCoords(bmp.Size).Y - interval.Radius;
						pen = new Pen(Var.TColorToColor(interval.BorderColor), interval.Radius / 10);
						brush = new SolidBrush(Var.TColorToColor(interval.FillColor));

						g.FillEllipse(brush, x, y, 2 * interval.Radius, 2 * interval.Radius);
						g.DrawEllipse(pen, x, y, 2 * interval.Radius, 2 * interval.Radius);

						Font font = Var.TFontToFont(interval.CaptionFont);
						brush = new SolidBrush(Var.TColorToColor(interval.CaptionFont.Color));

						x += interval.Radius;
						y += 2 * interval.Radius + 6;

						if (Var.Project.ShowCaptions)
						{
							g.DrawString(city.CityName, font, brush, x - g.MeasureString(city.CityName, font).Width / 2, y);
							y += (int) g.MeasureString(city.CityName, font).Height - 6;
						}

						if (Var.Project.ShowValues)
							g.DrawString(city.Value.ToString(), font, brush, x - g.MeasureString(city.Value.ToString(), font).Width / 2, y);
					}
					catch (Exception E)
					{
						log(Var.NL + "ERROR for city \"" + city.CityName + "\"! Continuing iteration..." + Var.NL + E.ToString() + Var.DNL);
					}

				// Legend again (to cover anything drawn over its area)
				DrawLegend(bmp, g, pen, brush);

				// Export
				log("Saving to file \"" + Path.GetFileName(outputPath) + "\"...");
				Phase = "Saving bitmap to output file";
				bmp.Save(outputPath);

				// Finalize
				log("");
				log("Exporting has finished successfully!");
			}
			catch (Exception E)
			{
				log("");
				log(Var.NL + "ERROR (phase \"" + Phase + "\"): " + E.ToString());
			}
		}

		private void editSettingsB_Click(object sender, EventArgs e)
		{
			Var.CityEditor.Hide();
			Var.IntervalEditor.Hide();
			Var.SettingsEditor.Hide();
			Var.SettingsEditor.Show();
			Var.SettingsEditor.RefreshInfo();
		}

		private void editIntervalsB_Click(object sender, EventArgs e)
		{
			Var.SettingsEditor.Hide();
			Var.CityEditor.Hide();
			Var.IntervalEditor.Hide();
			Var.IntervalEditor.Show();
			Var.IntervalEditor.RefreshInfo();
		}

		private void editCitiesB_Click(object sender, EventArgs e)
		{
			Var.SettingsEditor.Hide();
			Var.IntervalEditor.Hide();
			Var.CityEditor.Hide();
			Var.CityEditor.Show();
			Var.CityEditor.RefreshInfo();
		}
	}
}
