﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CityDistanceMap
{
	public partial class FCityEditor : Form
	{
		private TCity currCity = null;
		private double pcX = 0, pcY = 0;

		public FCityEditor()
		{
			InitializeComponent();
		}

		private void closeB_Click(object sender, EventArgs e)
		{
			Var.SaveProject(Var.ProjectPath);
			this.Hide();
		}

		public void RefreshInfo()
		{
			cityLB.Items.Clear();
			foreach (TCity city in Var.Project.Cities)
				cityLB.Items.Add(string.Format("{0} ({1})", city.CityName, city.Value));
			try { pic.Load(Var.ProjectsPath + "\\" + Var.Project.MapFilename); }
			catch (System.IO.FileNotFoundException FNFE)
			{
				MessageBox.Show("ERROR: the map file \"" + Var.ProjectsPath + "\\" + Var.Project.MapFilename + "\" could not be found!" + Var.DNL + "Go to the project settings and select an existing and correctly located map file!");
				this.Hide();
				return;
			}
			label6.Text = "Cities (" + cityLB.Items.Count.ToString() + ")";
			cityLB_SelectedIndexChanged(null, null);
		}

		private void RefreshPropCoords()
		{
			coordsTB.Text = TProportionalCoords.EncodeProportionalCoords(pcX, pcY);
		}

		private void cityLB_SelectedIndexChanged(object sender, EventArgs e)
		{
			nameTB.Enabled = cityLB.SelectedIndex != -1;
			nameTB.Text = "";
			valueTB.Enabled = nameTB.Enabled;
			valueTB.Text = "";
			coordsTB.Text = "-";

			delB.Enabled = nameTB.Enabled;
			saveB.Enabled = nameTB.Enabled;

			if (cityLB.SelectedIndex == -1)
				return;

			currCity = Var.Project.Cities[cityLB.SelectedIndex];
			nameTB.Text = currCity.CityName;
			valueTB.Text = currCity.Value.ToString();
			pcX = currCity.Coords.X;
			pcY = currCity.Coords.Y;
			RefreshPropCoords();

			nameTB.Focus();
		}

		private void pic_Click(object sender, EventArgs e)
		{
			if (cityLB.SelectedIndex == -1)
				return;

			Point clickP = pic.PointToClient(Cursor.Position);
			Point imageP = new Point();
			int w_i = pic.Image.Width;
			int h_i = pic.Image.Height;
			int w_c = pic.Width;
			int h_c = pic.Height;
			float imageRatio = w_i / (float) h_i; // image W:H ratio
			float containerRatio = w_c / (float) h_c; // container W:H ratio
			if (imageRatio >= containerRatio) // horizontal image
			{
				float scaleFactor = w_c / (float) w_i;
				float scaledHeight = h_i * scaleFactor;
				// calculate gap between top of container and top of image
				float filler = Math.Abs(h_c - scaledHeight) / 2;
				imageP.X = (int) (clickP.X / scaleFactor);
				imageP.Y = (int) ((clickP.Y - filler) / scaleFactor);
			}
			else // vertical image
			{
				float scaleFactor = h_c / (float) h_i;
				float scaledWidth = w_i * scaleFactor;
				float filler = Math.Abs(w_c - scaledWidth) / 2;
				imageP.X = (int) ((clickP.X - filler) / scaleFactor);
				imageP.Y = (int) (clickP.Y / scaleFactor);
			}

			if (imageP.X < 0 || imageP.Y < 0 || imageP.X >= pic.Image.Width || imageP.Y >= pic.Image.Height)
				return;

			pcX = (double) imageP.X / pic.Image.Width * 100;
			pcY = (double) imageP.Y / pic.Image.Height * 100;
			RefreshPropCoords();
		}

		private void saveB_Click(object sender, EventArgs e)
		{
			currCity.CityName = nameTB.Text;
			currCity.Value = Double.Parse(valueTB.Text);
			currCity.Coords = TProportionalCoords.DecodeProportionalCoords(coordsTB.Text);

			cityLB.Items[cityLB.SelectedIndex] = string.Format("{0} ({1})", currCity.CityName, currCity.Value);
			saveB.Enabled = false;
			saveB.Text = "Saved!";
			saveT.Enabled = true;
		}

		private void saveT_Tick(object sender, EventArgs e)
		{
			saveT.Enabled = false;
			saveB.Enabled = true;
			saveB.Text = "Save";
		}

		private void addB_Click(object sender, EventArgs e)
		{
			currCity = new TCity();
			currCity.CityName = "[New city]";
			currCity.Value = 20;

			Var.Project.Cities.Add(currCity);
			RefreshInfo();
			cityLB.SelectedIndex = cityLB.Items.Count - 1;
		}

		private void delB_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to delete the city \"" + currCity.CityName + "\" ?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				Var.Project.Cities.RemoveAt(cityLB.SelectedIndex);
				RefreshInfo();
			}
		}
	}
}
