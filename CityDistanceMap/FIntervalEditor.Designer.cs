﻿namespace CityDistanceMap
{
	partial class FIntervalEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.closeB = new System.Windows.Forms.Button();
			this.intervalLB = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.valueTB = new System.Windows.Forms.TextBox();
			this.valueTrB = new System.Windows.Forms.TrackBar();
			this.changeBorderColorB = new System.Windows.Forms.ListBox();
			this.changeFontB = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.radiusTrB = new System.Windows.Forms.TrackBar();
			this.radiusTB = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.changeFillColorB = new System.Windows.Forms.ListBox();
			this.captionTB = new System.Windows.Forms.TextBox();
			this.fillColorL = new System.Windows.Forms.Label();
			this.borderColorL = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.fontPreviewL = new System.Windows.Forms.Label();
			this.changeFontColorB = new System.Windows.Forms.ListBox();
			this.colorD = new System.Windows.Forms.ColorDialog();
			this.fontD = new System.Windows.Forms.FontDialog();
			this.saveB = new System.Windows.Forms.Button();
			this.delB = new System.Windows.Forms.Button();
			this.addB = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.saveT = new System.Windows.Forms.Timer(this.components);
			this.autoCaptionB = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.valueTrB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.radiusTrB)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// closeB
			// 
			this.closeB.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.closeB.ForeColor = System.Drawing.Color.Black;
			this.closeB.Location = new System.Drawing.Point(24, 381);
			this.closeB.Name = "closeB";
			this.closeB.Size = new System.Drawing.Size(150, 39);
			this.closeB.TabIndex = 13;
			this.closeB.Text = "Close";
			this.closeB.UseVisualStyleBackColor = true;
			this.closeB.Click += new System.EventHandler(this.closeB_Click);
			// 
			// intervalLB
			// 
			this.intervalLB.FormattingEnabled = true;
			this.intervalLB.ItemHeight = 21;
			this.intervalLB.Location = new System.Drawing.Point(24, 47);
			this.intervalLB.Name = "intervalLB";
			this.intervalLB.Size = new System.Drawing.Size(270, 319);
			this.intervalLB.TabIndex = 0;
			this.intervalLB.SelectedIndexChanged += new System.EventHandler(this.intervalLB_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(311, 50);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(97, 21);
			this.label1.TabIndex = 1;
			this.label1.Text = "Up to value:";
			// 
			// valueTB
			// 
			this.valueTB.Location = new System.Drawing.Point(430, 47);
			this.valueTB.Name = "valueTB";
			this.valueTB.Size = new System.Drawing.Size(234, 29);
			this.valueTB.TabIndex = 1;
			this.valueTB.Text = "abcde12345";
			this.valueTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// valueTrB
			// 
			this.valueTrB.AutoSize = false;
			this.valueTrB.Location = new System.Drawing.Point(670, 47);
			this.valueTrB.Maximum = 1073741824;
			this.valueTrB.Name = "valueTrB";
			this.valueTrB.Size = new System.Drawing.Size(257, 29);
			this.valueTrB.TabIndex = 2;
			this.valueTrB.Value = 1;
			this.valueTrB.Scroll += new System.EventHandler(this.valueTrB_Scroll);
			// 
			// changeBorderColorB
			// 
			this.changeBorderColorB.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.changeBorderColorB.Cursor = System.Windows.Forms.Cursors.Hand;
			this.changeBorderColorB.ForeColor = System.Drawing.Color.Transparent;
			this.changeBorderColorB.FormattingEnabled = true;
			this.changeBorderColorB.ItemHeight = 21;
			this.changeBorderColorB.Location = new System.Drawing.Point(430, 117);
			this.changeBorderColorB.Name = "changeBorderColorB";
			this.changeBorderColorB.Size = new System.Drawing.Size(42, 42);
			this.changeBorderColorB.TabIndex = 5;
			this.changeBorderColorB.Click += new System.EventHandler(this.changeBorderColorB_Click);
			// 
			// changeFontB
			// 
			this.changeFontB.Cursor = System.Windows.Forms.Cursors.Hand;
			this.changeFontB.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.changeFontB.ForeColor = System.Drawing.Color.Black;
			this.changeFontB.Location = new System.Drawing.Point(315, 290);
			this.changeFontB.Name = "changeFontB";
			this.changeFontB.Size = new System.Drawing.Size(93, 28);
			this.changeFontB.TabIndex = 8;
			this.changeFontB.Text = "Change font";
			this.changeFontB.UseVisualStyleBackColor = true;
			this.changeFontB.Click += new System.EventHandler(this.changeFontB_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(311, 85);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(107, 21);
			this.label2.TabIndex = 0;
			this.label2.Text = "Shape radius:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(311, 127);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(108, 21);
			this.label3.TabIndex = 10;
			this.label3.Text = "Border color:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(311, 216);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(71, 21);
			this.label4.TabIndex = 9;
			this.label4.Text = "Caption:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(311, 258);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(71, 21);
			this.label5.TabIndex = 12;
			this.label5.Text = "Preview:";
			// 
			// radiusTrB
			// 
			this.radiusTrB.AutoSize = false;
			this.radiusTrB.Location = new System.Drawing.Point(670, 82);
			this.radiusTrB.Maximum = 720;
			this.radiusTrB.Minimum = 4;
			this.radiusTrB.Name = "radiusTrB";
			this.radiusTrB.Size = new System.Drawing.Size(257, 29);
			this.radiusTrB.TabIndex = 4;
			this.radiusTrB.Value = 4;
			this.radiusTrB.Scroll += new System.EventHandler(this.radiusTrB_Scroll);
			// 
			// radiusTB
			// 
			this.radiusTB.Location = new System.Drawing.Point(430, 82);
			this.radiusTB.Name = "radiusTB";
			this.radiusTB.Size = new System.Drawing.Size(234, 29);
			this.radiusTB.TabIndex = 3;
			this.radiusTB.Text = "abcde12345";
			this.radiusTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(311, 175);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(77, 21);
			this.label7.TabIndex = 16;
			this.label7.Text = "Fill color:";
			// 
			// changeFillColorB
			// 
			this.changeFillColorB.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.changeFillColorB.Cursor = System.Windows.Forms.Cursors.Hand;
			this.changeFillColorB.ForeColor = System.Drawing.Color.Transparent;
			this.changeFillColorB.FormattingEnabled = true;
			this.changeFillColorB.ItemHeight = 21;
			this.changeFillColorB.Location = new System.Drawing.Point(430, 165);
			this.changeFillColorB.Name = "changeFillColorB";
			this.changeFillColorB.Size = new System.Drawing.Size(42, 42);
			this.changeFillColorB.TabIndex = 6;
			this.changeFillColorB.Click += new System.EventHandler(this.changeFillColorB_Click);
			// 
			// captionTB
			// 
			this.captionTB.Location = new System.Drawing.Point(430, 213);
			this.captionTB.Name = "captionTB";
			this.captionTB.Size = new System.Drawing.Size(429, 29);
			this.captionTB.TabIndex = 7;
			this.captionTB.Text = "abcde12345";
			// 
			// fillColorL
			// 
			this.fillColorL.AutoSize = true;
			this.fillColorL.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.fillColorL.Location = new System.Drawing.Point(478, 178);
			this.fillColorL.Name = "fillColorL";
			this.fillColorL.Size = new System.Drawing.Size(66, 19);
			this.fillColorL.TabIndex = 19;
			this.fillColorL.Text = "Fill color:";
			// 
			// borderColorL
			// 
			this.borderColorL.AutoSize = true;
			this.borderColorL.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.borderColorL.Location = new System.Drawing.Point(478, 130);
			this.borderColorL.Name = "borderColorL";
			this.borderColorL.Size = new System.Drawing.Size(88, 19);
			this.borderColorL.TabIndex = 18;
			this.borderColorL.Text = "Border color:";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.fontPreviewL);
			this.panel1.Location = new System.Drawing.Point(430, 248);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(497, 118);
			this.panel1.TabIndex = 20;
			// 
			// fontPreviewL
			// 
			this.fontPreviewL.AutoSize = true;
			this.fontPreviewL.Location = new System.Drawing.Point(211, 44);
			this.fontPreviewL.Name = "fontPreviewL";
			this.fontPreviewL.Size = new System.Drawing.Size(106, 21);
			this.fontPreviewL.TabIndex = 13;
			this.fontPreviewL.Text = "fontPreviewL";
			// 
			// changeFontColorB
			// 
			this.changeFontColorB.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.changeFontColorB.Cursor = System.Windows.Forms.Cursors.Hand;
			this.changeFontColorB.ForeColor = System.Drawing.Color.Transparent;
			this.changeFontColorB.FormattingEnabled = true;
			this.changeFontColorB.ItemHeight = 21;
			this.changeFontColorB.Location = new System.Drawing.Point(315, 324);
			this.changeFontColorB.Name = "changeFontColorB";
			this.changeFontColorB.Size = new System.Drawing.Size(93, 42);
			this.changeFontColorB.TabIndex = 9;
			this.changeFontColorB.Click += new System.EventHandler(this.changeFontColorB_Click);
			// 
			// colorD
			// 
			this.colorD.AnyColor = true;
			this.colorD.FullOpen = true;
			// 
			// fontD
			// 
			this.fontD.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.fontD.FontMustExist = true;
			this.fontD.MaxSize = 720;
			this.fontD.MinSize = 8;
			this.fontD.ShowEffects = false;
			// 
			// saveB
			// 
			this.saveB.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.saveB.ForeColor = System.Drawing.Color.Black;
			this.saveB.Location = new System.Drawing.Point(777, 381);
			this.saveB.Name = "saveB";
			this.saveB.Size = new System.Drawing.Size(150, 39);
			this.saveB.TabIndex = 10;
			this.saveB.Text = "Save";
			this.saveB.UseVisualStyleBackColor = true;
			this.saveB.Click += new System.EventHandler(this.saveB_Click);
			// 
			// delB
			// 
			this.delB.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.delB.ForeColor = System.Drawing.Color.Black;
			this.delB.Location = new System.Drawing.Point(621, 381);
			this.delB.Name = "delB";
			this.delB.Size = new System.Drawing.Size(150, 39);
			this.delB.TabIndex = 11;
			this.delB.Text = "Delete";
			this.delB.UseVisualStyleBackColor = true;
			this.delB.Click += new System.EventHandler(this.delB_Click);
			// 
			// addB
			// 
			this.addB.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.addB.ForeColor = System.Drawing.Color.Black;
			this.addB.Location = new System.Drawing.Point(465, 381);
			this.addB.Name = "addB";
			this.addB.Size = new System.Drawing.Size(150, 39);
			this.addB.TabIndex = 12;
			this.addB.Text = "Add new";
			this.addB.UseVisualStyleBackColor = true;
			this.addB.Click += new System.EventHandler(this.addB_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(19, 9);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(98, 30);
			this.label6.TabIndex = 25;
			this.label6.Text = "Intervals";
			// 
			// saveT
			// 
			this.saveT.Interval = 1500;
			this.saveT.Tick += new System.EventHandler(this.saveT_Tick);
			// 
			// autoCaptionB
			// 
			this.autoCaptionB.Cursor = System.Windows.Forms.Cursors.Hand;
			this.autoCaptionB.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.autoCaptionB.ForeColor = System.Drawing.Color.Black;
			this.autoCaptionB.Location = new System.Drawing.Point(865, 214);
			this.autoCaptionB.Name = "autoCaptionB";
			this.autoCaptionB.Size = new System.Drawing.Size(62, 28);
			this.autoCaptionB.TabIndex = 26;
			this.autoCaptionB.Text = "Auto";
			this.autoCaptionB.UseVisualStyleBackColor = true;
			this.autoCaptionB.Click += new System.EventHandler(this.button1_Click);
			// 
			// FIntervalEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.SteelBlue;
			this.ClientSize = new System.Drawing.Size(954, 437);
			this.ControlBox = false;
			this.Controls.Add(this.autoCaptionB);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.addB);
			this.Controls.Add(this.delB);
			this.Controls.Add(this.saveB);
			this.Controls.Add(this.changeFontColorB);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.fillColorL);
			this.Controls.Add(this.borderColorL);
			this.Controls.Add(this.captionTB);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.changeFillColorB);
			this.Controls.Add(this.radiusTrB);
			this.Controls.Add(this.radiusTB);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.changeFontB);
			this.Controls.Add(this.changeBorderColorB);
			this.Controls.Add(this.valueTrB);
			this.Controls.Add(this.valueTB);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.intervalLB);
			this.Controls.Add(this.closeB);
			this.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "FIntervalEditor";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Interval editor";
			((System.ComponentModel.ISupportInitialize)(this.valueTrB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.radiusTrB)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button closeB;
		private System.Windows.Forms.ListBox intervalLB;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox valueTB;
		private System.Windows.Forms.TrackBar valueTrB;
		private System.Windows.Forms.ListBox changeBorderColorB;
		private System.Windows.Forms.Button changeFontB;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TrackBar radiusTrB;
		private System.Windows.Forms.TextBox radiusTB;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ListBox changeFillColorB;
		private System.Windows.Forms.TextBox captionTB;
		private System.Windows.Forms.Label fillColorL;
		private System.Windows.Forms.Label borderColorL;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ListBox changeFontColorB;
		private System.Windows.Forms.ColorDialog colorD;
		private System.Windows.Forms.FontDialog fontD;
		public System.Windows.Forms.Label fontPreviewL;
		private System.Windows.Forms.Button saveB;
		private System.Windows.Forms.Button delB;
		private System.Windows.Forms.Button addB;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Timer saveT;
		private System.Windows.Forms.Button autoCaptionB;
	}
}