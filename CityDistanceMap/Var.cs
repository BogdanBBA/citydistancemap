﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace CityDistanceMap
{
	public static class Var
	{
		public static TProject Project = null;
		public static string ProjectPath = null;

		public static string ProjectsPath = null;
		public static readonly string NL = Environment.NewLine, DNL = NL + NL;

		public static readonly FIntervalEditor IntervalEditor = new FIntervalEditor();
		public static readonly FCityEditor CityEditor = new FCityEditor();
		public static readonly FSettingsEditor SettingsEditor = new FSettingsEditor();

		// Var: open project
		public static string OpenProject(string FilePath)
		{
			string phase = "reading data from XML: ";
			try
			{
				phase += "initializing the serializer; ";
				XmlSerializer serializer = new XmlSerializer(typeof(TProject));
				phase = "initializing the file stream; ";
				FileStream fs = new FileStream(FilePath, FileMode.Open);
				phase = "attempting deserialization";
				Project = (TProject) serializer.Deserialize(fs);
				fs.Close();
			}
			catch (Exception E)
			{
				return "Var.OpenProject(\"" + FilePath + "\") ERROR: Failed to open project at phase '" + phase + "'." + Var.DNL + E.ToString();
			}
			return "";
		}

		// Var: save project
		public static string SaveProject(string FilePath)
		{
			string phase = "saving data to XML: ";
			try
			{
				phase += "initializing the serializer; ";
				XmlSerializer serializer = new XmlSerializer(typeof(TProject));
				phase = "initializing the file stream; ";
				TextWriter writer = new StreamWriter(FilePath);
				phase = "attempting serialization; ";
				serializer.Serialize(writer, Project);
				writer.Close();
			}
			catch (Exception E)
			{
				return "Var.SaveProject(\"" + FilePath + "\") ERROR: Failed to save project at phase '" + phase + "'." + Var.DNL + E.ToString();
			}
			return "";
		}

		public static void CreateInitializatoryProject()
		{
			Project = new TProject();
			Project.Title = "T";
			Project.Subtitle = "St";
			Project.MapFilename = "map";
			TInterval i = new TInterval();
			i.Caption = "cap";
			i.UpToValue = 200.5d;
			i.FillColor.setColor(FMain.DefaultBackColor);
			Project.Intervals.Add(i);
			Project.Intervals.Add(i);
			TCity c = new TCity();
			c.CityName = "Brasov";
			c.Value = 20d;
			Project.Cities.Add(c);
			Project.Cities.Add(c);
			Project.Cities.Add(c);
			SaveProject(Var.ProjectsPath + "\\TestProject1.xml");
		}

		public static Color TColorToColor(TColor c)
		{
			return Color.FromArgb(c.R, c.G, c.B);
		}

		public static TColor ColorToTColor(Color c)
		{
			TColor res = new TColor();
			res.setColor(c);
			return res;
		}

		public static Font TFontToFont(TFont f)
		{
			FontStyle fsB = FontStyle.Regular;
			if (f.Bold)
				fsB = FontStyle.Bold;
			FontStyle fsI = FontStyle.Regular;
			if (f.Italic)
				fsI = FontStyle.Italic;
			Font res = new Font(f.FontName, (float) f.Size, fsB | fsI);
			return res;
		}

		public static TFont FontToTFont(Font f)
		{
			TFont res = new TFont();
			res.FontName = f.Name;
			res.Size = f.Size;
			res.Bold = f.Bold;
			res.Italic = f.Italic;
			return res;
		}
	}

	/// <summary>
	/// The Project class
	/// </summary>
	public class TProject
	{
		public const byte TopLeft = 1, TopRight = 2, BottomRight = 3, BottomLeft = 4;

		public string Title;
		public string Subtitle;
		public string MapFilename;
		public byte LegendPosition;
		public bool ShowCaptions = true, ShowValues = true, ShowLegend = true, ShowCityLegendConnection = false, ShowCityGravity = false;

		public List<TInterval> Intervals = new List<TInterval>();
		public List<TCity> Cities = new List<TCity>();

		public int GetIntervalIndexForValue(double Value)
		{
			for (int i = Intervals.Count - 1; i > 0; i--)
				if (Intervals[i - 1].UpToValue < Value && Value <= Intervals[i].UpToValue)
					return i;
			if (Intervals.Count > 0 && Value <= Intervals[0].UpToValue)
				return 0;
			MessageBox.Show("TProject.GetInterval(Index)ForValue() ERROR: could not find a suitable interval for value " + Value.ToString() + " !");
			return -1;
		}

		public TInterval GetIntervalForValue(double Value)
		{
			if (GetIntervalIndexForValue(Value) == -1)
				return null;
			return Intervals[GetIntervalIndexForValue(Value)];
		}

		public void PrepareData()
		{
			for (int i = 0; i < Intervals.Count - 1; i++)
				for (int j = i + 1; j < Intervals.Count; j++)
					if (Intervals[i].UpToValue > Intervals[j].UpToValue)
					{
						TInterval aux = Intervals[i];
						Intervals[i] = Intervals[j];
						Intervals[j] = aux;
					}
			for (int i = 0; i < Cities.Count - 1; i++)
				for (int j = i + 1; j < Cities.Count; j++)
					if (Cities[i].CityName.CompareTo(Cities[j].CityName) > 0)
					{
						TCity aux = Cities[i];
						Cities[i] = Cities[j];
						Cities[j] = aux;
					}
		}
	}

	/// <summary>
	/// The Interval class
	/// </summary>
	public class TInterval
	{
		public double UpToValue = 1d;
		public int Radius = 8;
		public TColor BorderColor = new TColor();
		public TColor FillColor = new TColor();
		public string Caption;
		public TFont CaptionFont = new TFont();

		public override string ToString()
		{
			return string.Format("TInterval(upToValue={0}, radius={1}, border {2}, fill {3}, caption '{4}', {5})", UpToValue, Radius, BorderColor, FillColor, Caption, CaptionFont);
		}
	}

	public class TColor
	{
		public byte R = 0, G = 0, B = 0;

		public void setColor(Color C)
		{
			R = C.R;
			G = C.G;
			B = C.B;
		}

		public override string ToString()
		{
			return string.Format("TColor(R={0}, G={1}, B={2})", R, G, B);
		}
	}

	public class TFont
	{
		public string FontName = "Seoge UI";
		public double Size = 10d;
		public TColor Color = new TColor();
		public bool Bold = false, Italic = false;

		public override string ToString()
		{
			return string.Format("TFont(name '{0}', size={1}, {2}, bold={3}, italic={4})", FontName, Size, Color, Bold, Italic);
		}
	}

	/// <summary>
	/// The City class
	/// </summary>
	public class TCity
	{
		public string CityName;
		public double Value;
		public TProportionalCoords Coords = new TProportionalCoords();

		public Point GetActualCoords(Size MapSize)
		{
			return new Point((int) (MapSize.Width * Coords.X / 100), (int) (MapSize.Height * Coords.Y / 100));
		}

		public override string ToString()
		{
			return string.Format("TCity(name '{0}', value={1}, {2})", CityName, Value, Coords);
		}
	}

	public class TProportionalCoords
	{
		public double X = 50d;
		public double Y = 50d;

		public static string EncodeProportionalCoords(TProportionalCoords Coords)
		{
			return EncodeProportionalCoords(Coords.X, Coords.Y);
		}

		public static string EncodeProportionalCoords(double X, double Y)
		{
			return string.Format("{0}% / {1}%", X.ToString("F", CultureInfo.InvariantCulture), Y.ToString("F", CultureInfo.InvariantCulture));
		}

		public static TProportionalCoords DecodeProportionalCoords(string FormattedText)
		{
			TProportionalCoords pc = new TProportionalCoords();
			string a = FormattedText.Substring(0, FormattedText.IndexOf("%"));
			FormattedText = FormattedText.Substring(FormattedText.IndexOf(" / ") + 3, FormattedText.Length - FormattedText.IndexOf(" / ") - 3);
			string b = FormattedText.Substring(0, FormattedText.IndexOf("%"));
			pc.X = Double.Parse(a);
			pc.Y = Double.Parse(b);
			return pc;
		}

		public override string ToString()
		{
			return string.Format("TProportionalCoords(x={0}, y={1})", X, Y);
		}
	}
}
