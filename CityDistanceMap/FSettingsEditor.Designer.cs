﻿namespace CityDistanceMap
{
	partial class FSettingsEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.saveB = new System.Windows.Forms.Button();
			this.closeB = new System.Windows.Forms.Button();
			this.saveT = new System.Windows.Forms.Timer(this.components);
			this.titleTB = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.subtitleTB = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.mapFilenameTB = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.nameChB = new System.Windows.Forms.CheckBox();
			this.valueChB = new System.Windows.Forms.CheckBox();
			this.selectMapB = new System.Windows.Forms.Button();
			this.legendPositionCB = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.openD = new System.Windows.Forms.OpenFileDialog();
			this.legendChB = new System.Windows.Forms.CheckBox();
			this.gravityChB = new System.Windows.Forms.CheckBox();
			this.connectionsChB = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// saveB
			// 
			this.saveB.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.saveB.ForeColor = System.Drawing.Color.Black;
			this.saveB.Location = new System.Drawing.Point(317, 319);
			this.saveB.Name = "saveB";
			this.saveB.Size = new System.Drawing.Size(150, 39);
			this.saveB.TabIndex = 7;
			this.saveB.Text = "Save";
			this.saveB.UseVisualStyleBackColor = true;
			this.saveB.Click += new System.EventHandler(this.saveB_Click);
			// 
			// closeB
			// 
			this.closeB.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.closeB.ForeColor = System.Drawing.Color.Black;
			this.closeB.Location = new System.Drawing.Point(162, 319);
			this.closeB.Name = "closeB";
			this.closeB.Size = new System.Drawing.Size(150, 39);
			this.closeB.TabIndex = 8;
			this.closeB.Text = "Close";
			this.closeB.UseVisualStyleBackColor = true;
			this.closeB.Click += new System.EventHandler(this.closeB_Click);
			// 
			// saveT
			// 
			this.saveT.Interval = 1500;
			this.saveT.Tick += new System.EventHandler(this.saveT_Tick);
			// 
			// titleTB
			// 
			this.titleTB.Location = new System.Drawing.Point(185, 63);
			this.titleTB.Name = "titleTB";
			this.titleTB.Size = new System.Drawing.Size(381, 29);
			this.titleTB.TabIndex = 33;
			this.titleTB.Text = "abcde12345";
			this.titleTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(46, 66);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(46, 21);
			this.label1.TabIndex = 34;
			this.label1.Text = "Title:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(26, 20);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(165, 30);
			this.label6.TabIndex = 35;
			this.label6.Text = "Project settings";
			// 
			// subtitleTB
			// 
			this.subtitleTB.Location = new System.Drawing.Point(185, 98);
			this.subtitleTB.Name = "subtitleTB";
			this.subtitleTB.Size = new System.Drawing.Size(381, 29);
			this.subtitleTB.TabIndex = 36;
			this.subtitleTB.Text = "abcde12345";
			this.subtitleTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(46, 101);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(71, 21);
			this.label2.TabIndex = 37;
			this.label2.Text = "Subtitle:";
			// 
			// mapFilenameTB
			// 
			this.mapFilenameTB.Location = new System.Drawing.Point(185, 167);
			this.mapFilenameTB.Name = "mapFilenameTB";
			this.mapFilenameTB.ReadOnly = true;
			this.mapFilenameTB.Size = new System.Drawing.Size(282, 29);
			this.mapFilenameTB.TabIndex = 38;
			this.mapFilenameTB.Text = "abcde12345";
			this.mapFilenameTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(46, 170);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(114, 21);
			this.label3.TabIndex = 39;
			this.label3.Text = "Map filename:";
			// 
			// nameChB
			// 
			this.nameChB.AutoSize = true;
			this.nameChB.Location = new System.Drawing.Point(185, 213);
			this.nameChB.Name = "nameChB";
			this.nameChB.Size = new System.Drawing.Size(150, 25);
			this.nameChB.TabIndex = 40;
			this.nameChB.Text = "Show city names";
			this.nameChB.UseVisualStyleBackColor = true;
			// 
			// valueChB
			// 
			this.valueChB.AutoSize = true;
			this.valueChB.Location = new System.Drawing.Point(185, 244);
			this.valueChB.Name = "valueChB";
			this.valueChB.Size = new System.Drawing.Size(148, 25);
			this.valueChB.TabIndex = 41;
			this.valueChB.Text = "Show city values";
			this.valueChB.UseVisualStyleBackColor = true;
			// 
			// selectMapB
			// 
			this.selectMapB.Cursor = System.Windows.Forms.Cursors.Hand;
			this.selectMapB.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.selectMapB.ForeColor = System.Drawing.Color.Black;
			this.selectMapB.Location = new System.Drawing.Point(473, 168);
			this.selectMapB.Name = "selectMapB";
			this.selectMapB.Size = new System.Drawing.Size(93, 28);
			this.selectMapB.TabIndex = 43;
			this.selectMapB.Text = "Select";
			this.selectMapB.UseVisualStyleBackColor = true;
			this.selectMapB.Click += new System.EventHandler(this.selectMapB_Click);
			// 
			// legendPositionCB
			// 
			this.legendPositionCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.legendPositionCB.FormattingEnabled = true;
			this.legendPositionCB.Items.AddRange(new object[] {
            "Top-left corner",
            "Top-right corner",
            "Bottom-right corner",
            "Bottom-left corner"});
			this.legendPositionCB.Location = new System.Drawing.Point(185, 133);
			this.legendPositionCB.Name = "legendPositionCB";
			this.legendPositionCB.Size = new System.Drawing.Size(381, 29);
			this.legendPositionCB.TabIndex = 44;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(46, 136);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(133, 21);
			this.label4.TabIndex = 45;
			this.label4.Text = "Legend position:";
			// 
			// openD
			// 
			this.openD.FileName = "openFileDialog1";
			this.openD.Filter = "Bitmap files|*.bmp";
			this.openD.InitialDirectory = "projects";
			this.openD.Title = "Select map (only bitmaps located in the \"projects\" folder!)";
			this.openD.FileOk += new System.ComponentModel.CancelEventHandler(this.openD_FileOk);
			// 
			// legendChB
			// 
			this.legendChB.AutoSize = true;
			this.legendChB.Location = new System.Drawing.Point(185, 275);
			this.legendChB.Name = "legendChB";
			this.legendChB.Size = new System.Drawing.Size(124, 25);
			this.legendChB.TabIndex = 46;
			this.legendChB.Text = "Show legend";
			this.legendChB.UseVisualStyleBackColor = true;
			// 
			// gravityChB
			// 
			this.gravityChB.AutoSize = true;
			this.gravityChB.Location = new System.Drawing.Point(351, 244);
			this.gravityChB.Name = "gravityChB";
			this.gravityChB.Size = new System.Drawing.Size(203, 25);
			this.gravityChB.TabIndex = 48;
			this.gravityChB.Text = "Show city gravity circles";
			this.gravityChB.UseVisualStyleBackColor = true;
			// 
			// connectionsChB
			// 
			this.connectionsChB.AutoSize = true;
			this.connectionsChB.Location = new System.Drawing.Point(351, 213);
			this.connectionsChB.Name = "connectionsChB";
			this.connectionsChB.Size = new System.Drawing.Size(242, 25);
			this.connectionsChB.TabIndex = 47;
			this.connectionsChB.Text = "Show city-legend connection";
			this.connectionsChB.UseVisualStyleBackColor = true;
			// 
			// FSettingsEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.SteelBlue;
			this.ClientSize = new System.Drawing.Size(605, 378);
			this.ControlBox = false;
			this.Controls.Add(this.gravityChB);
			this.Controls.Add(this.connectionsChB);
			this.Controls.Add(this.legendChB);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.legendPositionCB);
			this.Controls.Add(this.selectMapB);
			this.Controls.Add(this.valueChB);
			this.Controls.Add(this.nameChB);
			this.Controls.Add(this.mapFilenameTB);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.subtitleTB);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.titleTB);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.saveB);
			this.Controls.Add(this.closeB);
			this.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "FSettingsEditor";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Settings editor";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button saveB;
		private System.Windows.Forms.Button closeB;
		private System.Windows.Forms.Timer saveT;
		private System.Windows.Forms.TextBox titleTB;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox subtitleTB;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox mapFilenameTB;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox nameChB;
		private System.Windows.Forms.CheckBox valueChB;
		private System.Windows.Forms.Button selectMapB;
		private System.Windows.Forms.ComboBox legendPositionCB;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.OpenFileDialog openD;
		private System.Windows.Forms.CheckBox legendChB;
		private System.Windows.Forms.CheckBox gravityChB;
		private System.Windows.Forms.CheckBox connectionsChB;
	}
}