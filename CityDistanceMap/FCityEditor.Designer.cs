﻿namespace CityDistanceMap
{
	partial class FCityEditor
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label6 = new System.Windows.Forms.Label();
			this.cityLB = new System.Windows.Forms.ListBox();
			this.closeB = new System.Windows.Forms.Button();
			this.addB = new System.Windows.Forms.Button();
			this.delB = new System.Windows.Forms.Button();
			this.saveB = new System.Windows.Forms.Button();
			this.nameTB = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.valueTB = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.coordsTB = new System.Windows.Forms.Label();
			this.pic = new System.Windows.Forms.PictureBox();
			this.saveT = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
			this.SuspendLayout();
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(21, 9);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(98, 30);
			this.label6.TabIndex = 28;
			this.label6.Text = "Intervals";
			// 
			// cityLB
			// 
			this.cityLB.FormattingEnabled = true;
			this.cityLB.ItemHeight = 21;
			this.cityLB.Location = new System.Drawing.Point(26, 47);
			this.cityLB.Name = "cityLB";
			this.cityLB.Size = new System.Drawing.Size(270, 529);
			this.cityLB.TabIndex = 0;
			this.cityLB.SelectedIndexChanged += new System.EventHandler(this.cityLB_SelectedIndexChanged);
			// 
			// closeB
			// 
			this.closeB.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.closeB.ForeColor = System.Drawing.Color.Black;
			this.closeB.Location = new System.Drawing.Point(26, 598);
			this.closeB.Name = "closeB";
			this.closeB.Size = new System.Drawing.Size(150, 39);
			this.closeB.TabIndex = 6;
			this.closeB.Text = "Close";
			this.closeB.UseVisualStyleBackColor = true;
			this.closeB.Click += new System.EventHandler(this.closeB_Click);
			// 
			// addB
			// 
			this.addB.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.addB.ForeColor = System.Drawing.Color.Black;
			this.addB.Location = new System.Drawing.Point(459, 598);
			this.addB.Name = "addB";
			this.addB.Size = new System.Drawing.Size(150, 39);
			this.addB.TabIndex = 5;
			this.addB.Text = "Add new";
			this.addB.UseVisualStyleBackColor = true;
			this.addB.Click += new System.EventHandler(this.addB_Click);
			// 
			// delB
			// 
			this.delB.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.delB.ForeColor = System.Drawing.Color.Black;
			this.delB.Location = new System.Drawing.Point(615, 598);
			this.delB.Name = "delB";
			this.delB.Size = new System.Drawing.Size(150, 39);
			this.delB.TabIndex = 4;
			this.delB.Text = "Delete";
			this.delB.UseVisualStyleBackColor = true;
			this.delB.Click += new System.EventHandler(this.delB_Click);
			// 
			// saveB
			// 
			this.saveB.Font = new System.Drawing.Font("Segoe UI", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.saveB.ForeColor = System.Drawing.Color.Black;
			this.saveB.Location = new System.Drawing.Point(771, 598);
			this.saveB.Name = "saveB";
			this.saveB.Size = new System.Drawing.Size(150, 39);
			this.saveB.TabIndex = 3;
			this.saveB.Text = "Save";
			this.saveB.UseVisualStyleBackColor = true;
			this.saveB.Click += new System.EventHandler(this.saveB_Click);
			// 
			// nameTB
			// 
			this.nameTB.Location = new System.Drawing.Point(429, 47);
			this.nameTB.Name = "nameTB";
			this.nameTB.Size = new System.Drawing.Size(492, 29);
			this.nameTB.TabIndex = 1;
			this.nameTB.Text = "abcde12345";
			this.nameTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(310, 50);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(86, 21);
			this.label1.TabIndex = 32;
			this.label1.Text = "City name:";
			// 
			// valueTB
			// 
			this.valueTB.Location = new System.Drawing.Point(429, 82);
			this.valueTB.Name = "valueTB";
			this.valueTB.Size = new System.Drawing.Size(492, 29);
			this.valueTB.TabIndex = 2;
			this.valueTB.Text = "abcde12345";
			this.valueTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(310, 85);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(89, 21);
			this.label2.TabIndex = 34;
			this.label2.Text = "Data value:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(310, 120);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(109, 21);
			this.label3.TabIndex = 36;
			this.label3.Text = "Coordonates:";
			// 
			// coordsTB
			// 
			this.coordsTB.AutoSize = true;
			this.coordsTB.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.coordsTB.Location = new System.Drawing.Point(425, 120);
			this.coordsTB.Name = "coordsTB";
			this.coordsTB.Size = new System.Drawing.Size(96, 21);
			this.coordsTB.TabIndex = 37;
			this.coordsTB.Text = "Data value:";
			// 
			// pic
			// 
			this.pic.Cursor = System.Windows.Forms.Cursors.Cross;
			this.pic.Location = new System.Drawing.Point(314, 153);
			this.pic.Name = "pic";
			this.pic.Size = new System.Drawing.Size(607, 423);
			this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pic.TabIndex = 38;
			this.pic.TabStop = false;
			this.pic.Click += new System.EventHandler(this.pic_Click);
			// 
			// saveT
			// 
			this.saveT.Interval = 1500;
			this.saveT.Tick += new System.EventHandler(this.saveT_Tick);
			// 
			// FCityEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.SteelBlue;
			this.ClientSize = new System.Drawing.Size(942, 656);
			this.ControlBox = false;
			this.Controls.Add(this.pic);
			this.Controls.Add(this.coordsTB);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.valueTB);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.nameTB);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.addB);
			this.Controls.Add(this.delB);
			this.Controls.Add(this.saveB);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.cityLB);
			this.Controls.Add(this.closeB);
			this.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.White;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "FCityEditor";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "City editor";
			((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ListBox cityLB;
		private System.Windows.Forms.Button closeB;
		private System.Windows.Forms.Button addB;
		private System.Windows.Forms.Button delB;
		private System.Windows.Forms.Button saveB;
		private System.Windows.Forms.TextBox nameTB;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox valueTB;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label coordsTB;
		private System.Windows.Forms.PictureBox pic;
		private System.Windows.Forms.Timer saveT;
	}
}